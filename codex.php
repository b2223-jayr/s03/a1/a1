<?php

class Person{

	
	public $firstName;
	public $lastName;
	public $middleName;

	
	public function __construct($firstName, $lastName, $middleName){
	
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->middleName = $middleName;
	}

	// Method
	public function printName(){
		return "Your Full name is $this->firstName $this->middleName $this->lastName";
	}
}

$person = new Person('John','Smith','Middleton');


class Developer extends Person{

	public function printName(){
		return "your name is $this->firstName  $this->middleName $this->lastName and you are a developer";
	}
}

$developer = new Developer('Jayr', 'Padua', 'Gallardo');


class Engineer extends Person{
	public function printName(){
		return "you are an engineer named $this->firstName  $this->middleName $this->lastName";
	}
}

$engineer = new Engineer('mike', 'enriquez', 'SuckerBerj');
